#include "stdafx.h"
#include "CLine.h"


CLine::CLine()
{
}


CLine::~CLine()
{
}


void CLine::Draw(HDC hdc)
{
	MoveToEx(hdc, lefttop.x, lefttop.y, NULL);
	LineTo(hdc, rightbottom.x, rightbottom.y);
}

CShape* CLine::Create() {
	return new CLine;
}

void CLine::setCoor(POINT lefttop, POINT rightbottom)
{
	this->lefttop.x = lefttop.x;
	this->lefttop.y = lefttop.y;
	this->rightbottom.x = rightbottom.x;
	this->rightbottom.y = rightbottom.y;
}