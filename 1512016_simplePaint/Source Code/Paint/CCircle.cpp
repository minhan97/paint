#include "stdafx.h"
#include "CCircle.h"


CCircle::CCircle()
{
}


CCircle::~CCircle()
{
}


void CCircle::Draw(HDC hdc)
{
	RECT a;
	a.left = min(lefttop.x, lefttop.y);
	a.top = min(rightbottom.x, rightbottom.y);
	a.right = max(lefttop.x, lefttop.y);
	a.bottom = max(rightbottom.x, rightbottom.y);

	int length = min(abs(lefttop.y - lefttop.x), abs(rightbottom.y - rightbottom.x));

	if (lefttop.x > lefttop.y)
	{
		a.left = a.right - length;
	}
	else
		a.right = a.left + length;

	if (rightbottom.y < rightbottom.x)
	{
		a.top = a.bottom - length;
	}
	else
	{
		a.bottom = a.top + length;
	}

	Ellipse(hdc, a.left, a.top, a.right, a.bottom);

}

CShape* CCircle::Create() {
	return new CCircle();
}

void CCircle::setCoor(POINT lefttop, POINT rightbottom)
{
	this->lefttop.x = lefttop.x;
	this->lefttop.y = lefttop.y;
	this->rightbottom.x = rightbottom.x;
	this->rightbottom.y = rightbottom.y;
}