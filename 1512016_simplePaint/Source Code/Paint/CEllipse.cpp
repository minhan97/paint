#include "stdafx.h"
#include "CEllipse.h"


CEllipse::CEllipse()
{
}


CEllipse::~CEllipse()
{
}

void CEllipse::Draw(HDC hdc)
{
	Ellipse(hdc, lefttop.x, lefttop.y, rightbottom.x, rightbottom.y);
}

CShape* CEllipse::Create() {
	return new CEllipse();
}

void CEllipse::setCoor(POINT lefttop, POINT rightbottom)
{
	this->lefttop.x = lefttop.x;
	this->lefttop.y = lefttop.y;
	this->rightbottom.x = rightbottom.x;
	this->rightbottom.y = rightbottom.y;
}