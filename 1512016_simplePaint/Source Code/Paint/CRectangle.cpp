#include "stdafx.h"
#include "CRectangle.h"


CRectangle::CRectangle()
{
}


CRectangle::~CRectangle()
{
}

void CRectangle::Draw(HDC hdc)
{
	Rectangle(hdc, lefttop.x, lefttop.y, rightbottom.x, rightbottom.y);
}

CShape* CRectangle::Create() {
	return new CRectangle();
}

void CRectangle::setCoor(POINT lefttop, POINT rightbottom)
{
	this->lefttop.x = lefttop.x;
	this->lefttop.y = lefttop.y;
	this->rightbottom.x = rightbottom.x;
	this->rightbottom.y = rightbottom.y;
}