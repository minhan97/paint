#pragma once
#include "CShape.h"
class CSquare :
	public CShape
{
public:
	CSquare();
	~CSquare();

	void Draw(HDC hdc);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
};

