#pragma once
#include "CShape.h"
class CLine :
	public CShape
{
public:
	CLine();
	~CLine();

	void Draw(HDC hdc);

	CShape* Create();

	void setCoor(POINT lefttop, POINT rightbottom);
};

