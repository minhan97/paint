#pragma once
class CShape
{
protected:
	POINT lefttop;
	POINT rightbottom;
public:
	CShape();
	~CShape();

	virtual void Draw(HDC hdc) = 0;
	virtual CShape* Create() = 0;
	virtual void setCoor(POINT lefttop, POINT rightbottom) = 0;
};

