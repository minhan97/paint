#pragma once
#include "CShape.h"
class CCircle :
	public CShape
{
public:
	CCircle();
	~CCircle();

	void Draw(HDC hdc);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
};

