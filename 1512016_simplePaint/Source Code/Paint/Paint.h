#pragma once

#include "resource.h"
#include "CCircle.h"
#include "CEllipse.h"
#include "CLine.h"
#include "CRectangle.h"
#include "CShape.h"
#include "CSquare.h"