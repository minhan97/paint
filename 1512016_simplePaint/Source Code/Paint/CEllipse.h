#pragma once
#include "CShape.h"
class CEllipse :
	public CShape
{
public:
	CEllipse();
	~CEllipse();

	void Draw(HDC hdc);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
};

