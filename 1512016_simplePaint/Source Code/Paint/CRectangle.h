#pragma once
#include "CShape.h"
class CRectangle :
	public CShape
{
public:
	CRectangle();
	~CRectangle();

	void Draw(HDC hdc);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
};

