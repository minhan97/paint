THÔNG TIN CÁ NHÂN
- Họ tên: Lê Minh Ân
- MSSV: 1512016
- Email: anleminh97@gmail.com

CÁC CHỨC NĂNG ĐÃ LÀM:
- Vẽ 5 các hình cơ bản
- Chọn hình vẽ từ menu
- Check menu
- Giữ SHIFT để vẽ hình tròn và hình vuông.
- Bọc các đối tượng vào lớp model
- Sử dụng đa hình để cài đặt việc quản lý các đối tượng và vẽ hình

LINK DEMO YOUTUBE:
https://youtu.be/2h6qlKEn7mo

LINK BITBUCKET:
https://minhan97@bitbucket.org/minhan97/paint.git